export default [
  {
    id: 1,
    color: '#73B6EA',
    checked: true,
  },
  {
    id: 2,
    color: '#FFBE15',
    checked: false,
  },
  {
    id: 3,
    color: '#939393',
    checked: false,
  },
  {
    id: 4,
    color: '#000',
    checked: false,
  },
  {
    id: 5,
    color: '#FF6B00',
    checked: false,
  },
];
